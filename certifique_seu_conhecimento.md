# 🏆 Certifique seu Conhecimento
## Fundamentos de Orientação a Objetos com Kotlin

#### Marque a alternativa que deixa claro e corretamente o conceito de classe base e sub-classe:

* [x] Classe base é a classe que concede as características e a sub-classe herda as características da classe base

#### É correto afirmar sobre interface :

* [x] Pode extender de outras interfaces 

####  É correto afirmar sobre Herança :

* [x] Herança é] um mecanismo que permite que características comuns a diversas classes sejam fatoradas em uma classe base, ou superclasse. A partir de uma classe base, outras classes podem ser especificadas 

#### É correto afirmar sobre Polimorfismo :

* [x] Em programação orientada a objetos, polimorfismo é o principio pelo qual duas ou mais classes derivadas da mesma superclasse podem invocar métodos que tem a mesma assinatura, mas comportamentos distintos;

#### Sobre data class é correto afirmar que :

* [x] Data Class pode implementar interface e com o uso de copy pode-se obter uma nova instancia e então alterar nessa instancia o estado od objeto

#### Sobre Enum, podemos afirmar : 

* [x] Assim como qualquer classe, uma enum pode ter construtor e propriedade

#### Sobre Classe, é correto afirmar que : 

* [x] é um artefato que permite representar uma abstração do mundo real

#### É correto afirmar sobre classe abstrata : 

* [x] Uma classe abstrata pode herdar de uma unica outra classe abstrata e implementar várias interfaces

#### Quais das alternativas abaixo atende de maneira corrreta a explicação sobre membros de uma classe : 

* [x] São propriedades, construtores e funções de uma classe

#### Dentre as alternativas abaixo, qual delas realmente deixa explícito o propósito do paradigma de orientação a objetos : 

* [x] Utilizamos o paradigma orientado a objetos para definirmos classes, propriedades e métodos que representem uma situação real

