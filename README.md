# Fundamentos de Orientação a Objetos com Kotlin

Nesse módulo é desenvolvido um projeto guiado a testes para desenvolvimento do conteúdo orientado a objetos, nas quais foram criadas classes e classes de testes que executam testes de conjuntos de classes ou classes individualmente sempre usando o paradigma de orientação a objetos.

## 🧰 Ferramentas Utilizadas

* [IntelliJ Community](https://www.jetbrains.com/pt-br/idea/)

## 📚 Tópicos abordados
### 📙 **Conceito e prática sobre classe**

* [x] Apresentação do conteúdo do curso
* [x] Determinando o projeto prático
* [x] Introdução a Orientação a objetos em Kotlin
* [x] Classe pública e classe privada
* [x] Entendendo sobre classes na prática e criando o projeto
* [x] O que são membros de uma classe
* [x] Membros das classes na prática - Parte 1
* [x] Membros das classes na prática - Parte 2
* [x] Membros das classes na prática - Parte 3
* [x] Membros das classes na prática - Parte 4
* [x] Conceitos sobre data class
* [x] Entendendo sobre data class na prática

### 📙 **Avançando com Enum, abstrações e polimorfismo**

* [x] Introdução a Enum
* [x] Entendendo sobre Enum na prática - Parte 1
* [x] Entendendo sobre Enum na prática - Parte 2
* [x] Introdução a Abstrações
* [x] Entendendo sobre abstrações na prática - Parte 1
* [x] Entendendo sobre abstrações na prática - Parte 2
* [x] Entendendo sobre abstrações na prática - Parte 3
* [x] Introdução a Polimorfismo
* [x] Entendendo sobre polimorfismo na prática
* [x] Conclusão do curso
* [x] [Certifique seu conhecimento](https://gitlab.com/cursos-digital-innovation-one/fundamentos-de-orientacao-a-objetos-com-kotlin/-/blob/main/certifique_seu_conhecimento.md)

## [Projeto digionebank](https://gitlab.com/cursos-digital-innovation-one/fundamentos-de-orientacao-a-objetos-com-kotlin/-/tree/main/digionebank)
